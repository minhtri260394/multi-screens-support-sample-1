package teamkotlin.sample.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.layout_option_item.view.*
import teamkotlin.sample.R

class OptionAdapter(val items : ArrayList<String>, val context: Context) : RecyclerView.Adapter<OptionAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, pos: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_option_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.txtvOption?.text = items.get(position)
    }


    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val txtvOption = view.txtvOption
    }
}