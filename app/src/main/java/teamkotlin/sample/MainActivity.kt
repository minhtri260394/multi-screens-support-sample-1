package teamkotlin.sample

import android.annotation.SuppressLint
import android.app.FragmentTransaction
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.roughike.bottombar.BottomBar
import com.roughike.bottombar.BottomBarTab
import kotlinx.android.synthetic.main.activity_main.*
import teamkotlin.sample.R.id.bottomBar
import teamkotlin.sample.Views.TabNearby.NearbyFragment


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var bottomBar: BottomBar = findViewById(R.id.bottomBar)
        bottomBar.getTabAtPosition(2).inActiveColor  = resources.getColor(R.color.colorPrimary)
        val fragmentHome = NearbyFragment()
        val fm = supportFragmentManager

        switchFragment(fm, fragmentHome, "home", true)
    }

    internal fun switchFragment(
        fm: android.support.v4.app.FragmentManager,
        frag: android.support.v4.app.Fragment,
        TAG: String,
        isRemoveInBackStack: Boolean
    ) {
        val ft = fm.beginTransaction()
        val existsFrag = fm.findFragmentByTag(TAG)
        if (existsFrag != null) {
            val ftRemove = fm.beginTransaction()
            ftRemove.remove(existsFrag)
            ftRemove.commit()
        }
        ft.replace(R.id.llContainter, frag, TAG)
        ft.addToBackStack(TAG)
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        ft.commit()
    }

    override fun onBackPressed() {

        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

}
