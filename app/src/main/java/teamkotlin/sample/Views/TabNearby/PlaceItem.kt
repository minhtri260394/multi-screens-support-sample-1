package teamkotlin.sample.Views.TabNearby

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import teamkotlin.sample.DTO.PlaceDTO
import teamkotlin.sample.R
import teamkotlin.sample.helper.ImageHelper

class PlaceItem : FrameLayout {
    internal val mContext: Context
    var imgLogo : ImageView? = null
    var txtvDistance : TextView? = null
    var txtvMoney : TextView? = null
    var txtvRating : TextView? = null
    var ratingBar : RatingBar? = null
    var txtvPlaceName : TextView? = null
    var txtvOffPercent : TextView? = null


    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        mContext = context
        initMenu()
    }
    constructor(context: Context) : super(context) {
        mContext = context
        initMenu()
    }

    private fun initMenu() {
//        val vi = mContext
//            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
//
//        val view = vi.inflate(R.layout.layout_place_item, null)

        val view = LayoutInflater.from(mContext).inflate(R.layout.layout_place_item, this)

        imgLogo = view.findViewById(R.id.imgLogo)
        txtvDistance = view.findViewById(R.id.txtvDistance)
        txtvMoney = view.findViewById(R.id.txtvMoney)
        ratingBar = view.findViewById(R.id.ratingBar)
        txtvRating = view.findViewById(R.id.txtvRating)
        txtvPlaceName = view.findViewById(R.id.txtvPlaceName)
        txtvOffPercent = view.findViewById(R.id.txtvOffPercent)
    }

    fun setData(data: PlaceDTO) {
        ImageHelper.get().load(data.image)
            .fit()
            .transform(ImageHelper.getRoundedBorderTransformation(mContext))
            .into(imgLogo)
        txtvDistance!!.text = data.distance.toString()
        txtvMoney?.text = "$ " + data.price.toString()
        txtvRating?.text = data.ratting.toString()
        ratingBar?.rating = data.ratting
        txtvPlaceName?.text  = data.name
        txtvOffPercent?.text = data.OffRatio.toString()
    }
}
