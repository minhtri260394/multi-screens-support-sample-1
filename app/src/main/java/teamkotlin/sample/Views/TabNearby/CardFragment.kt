package teamkotlin.sample.Views.TabNearby

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_viewpager.*
import teamkotlin.sample.Adapter.CardAdapter
import teamkotlin.sample.R
import teamkotlin.sample.Util.TouchEffect


class CardFragment : Fragment() {

    var cardView: CardView? = null
        private set

    @SuppressLint("DefaultLocale")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.item_viewpager, container, false)

        cardView = view.findViewById<View>(R.id.cardView) as CardView
        cardView!!.maxCardElevation = cardView!!.cardElevation * CardAdapter.MAX_ELEVATION_FACTOR

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        TouchEffect.attach(imgFood)
        TouchEffect.attach(imgMarket)
        TouchEffect.attach(imgMovie)
        TouchEffect.attach(imgKTV)
        TouchEffect.attach(imgTour)
        TouchEffect.attach(imgHotel)
        TouchEffect.attach(imgBeauty)
        TouchEffect.attach(imgGym)
    }

    companion object {

        fun getInstance(position: Int): Fragment {
            val f = CardFragment()
            val args = Bundle()
            args.putInt("position", position)
            f.arguments = args

            return f
        }
    }
}
