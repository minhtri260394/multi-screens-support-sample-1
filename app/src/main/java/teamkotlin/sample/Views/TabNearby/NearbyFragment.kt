package teamkotlin.sample.Views.TabNearby


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import teamkotlin.sample.R
import teamkotlin.sample.Util.ShadowTransformer
import teamkotlin.sample.Adapter.CardFragmentPagerAdapter
import kotlinx.android.synthetic.main.fragment_nearby.*
import teamkotlin.sample.Adapter.OptionAdapter
import teamkotlin.sample.DTO.PlaceDTO


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class NearbyFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_nearby, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        ///menu
        val pagerAdapter = CardFragmentPagerAdapter(fragmentManager, dpToPixels(2, this.context!!))
        val fragmentCardShadowTransformer = ShadowTransformer(viewPager, pagerAdapter)
        fragmentCardShadowTransformer.enableScaling(true)
        viewPager.adapter = pagerAdapter
        viewPager.setPageTransformer(false, fragmentCardShadowTransformer)
        viewPager.offscreenPageLimit = 3


//option

        var listOption :ArrayList<String> = ArrayList()
        listOption.add("EDITOR'S CHOICE")
        listOption.add("POPULAR")
        listOption.add("CHART")
        listOption.add("RECOMMEND")
        val optionAdapter = OptionAdapter(listOption,context!!)
        recyclerView?.adapter = optionAdapter
        optionAdapter.notifyDataSetChanged()

//place
        val mcDonald = PlaceItem(this.context!!, null)
        mcDonald.setData(PlaceDTO("MC Donald",150,30,3.5F,20,R.drawable.logo_mcdonald))
        val layoutParams : ViewGroup.LayoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
            ,ViewGroup.LayoutParams.WRAP_CONTENT)
        mcDonald.layoutParams = layoutParams

        val kfc = PlaceItem(this.context!!, null)
        kfc.setData(PlaceDTO("KFC",500,45,4F,34,R.drawable.ic_kfc))
        kfc.layoutParams = layoutParams

        val theCoffeeHouse = PlaceItem(this.context!!, null)
        theCoffeeHouse.setData(PlaceDTO("The Coffee House",200,29,4.5F,10,R.drawable.ic_the_coffee_house))
        theCoffeeHouse.layoutParams = layoutParams

        llPlace.addView(mcDonald)
        llPlace.addView(kfc)
        llPlace.addView(theCoffeeHouse)



    }

    fun dpToPixels(dp: Int, context: Context): Float {
        return dp * context.resources.displayMetrics.density
    }
}
