package teamkotlin.sample.Util

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.os.Build
import android.view.MotionEvent
import android.view.View
import android.view.animation.AlphaAnimation

/**
 *
 * View Press Effect Helper - do some simple press effect like iOS
 *
 * Copyright (c) 2014 @author extralam @ HongKong (http://ah-lam.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Simple Usage: ImageView img = (ImageView) findViewById(R.id.img);
 * ViewPressEffectHelper.attach(img)
 *
 *
 *
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
@SuppressLint("NewApi")
object TouchEffect {

    /**
     * Attach the View which you want have a touch event
     *
     * @param view
     * - any view
     */
    fun attach(view: View) {
        view.setOnTouchListener(ASetOnTouchListener(view))
    }

    /**
     * Attach the View which you want have a touch event and listener
     * @param view
     * @param onClickListener
     */
    fun attach(view: View, onClickListener: View.OnClickListener) {
        view.setOnTouchListener(ASetOnTouchListener(view))
        view.setOnClickListener(onClickListener)
    }

    private class ASetOnTouchListener(v: View) : View.OnTouchListener {

        internal val ZERO_ALPHA = 1.0f
        internal val HALF_ALPHA = 0.3f
        internal val FIXED_DURATION = 50
        internal var alphaOrginally = 1.0f

        init {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                alphaOrginally = v.alpha
            }
        }

        @SuppressLint("ClickableViewAccessibility")
        override fun onTouch(v: View, event: MotionEvent): Boolean {
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                        val animation = AlphaAnimation(
                            ZERO_ALPHA, HALF_ALPHA
                        )
                        animation.duration = FIXED_DURATION.toLong()
                        animation.fillAfter = true
                        v.startAnimation(animation)
                    } else {
                        v.animate().setDuration(FIXED_DURATION.toLong()).alpha(HALF_ALPHA)
                    }
                }
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                        val animation = AlphaAnimation(
                            HALF_ALPHA, ZERO_ALPHA
                        )
                        animation.duration = FIXED_DURATION.toLong()
                        animation.fillAfter = true
                        v.startAnimation(animation)
                    } else {
                        v.animate().setDuration(FIXED_DURATION.toLong())
                            .alpha(alphaOrginally)
                    }
                }
            }
            return false
        }

    }
}